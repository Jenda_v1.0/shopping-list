import * as React from "react";
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core";
import TextField from "@material-ui/core/TextField/TextField";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";


const styles = theme => ({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit
    }
});


/**
 * Komponenta sloužící jako textové pole typu "select" - rozbalovací nabídka. V nabídce budou vždy hodnoty v "this.props.items".
 *
 * Dále je v props třeba předat label, value, helperText, width a funkci handleChange, která se vykoná po označení jedné z nabídky.
 */

class SelectTextField extends React.Component {

    render() {
        return (
            <TextField
                id="item"
                select
                label={this.props.label}
                className={this.props.classes.textField}
                value={this.props.value}
                onChange={this.props.handleChange}
                helperText={this.props.helperText}
                margin="normal"
                variant="filled"
                fullWidth={false}
                style={{width: this.props.width}}
            >
                {/*Nastavení položek do menu:*/}
                {this.props.items.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </TextField>
        );
    }
}


SelectTextField.propTypes = {
    classes: PropTypes.object.isRequired
};


export default withStyles(styles)(SelectTextField);