import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';


const styles = ({
    button: {
        margin: "auto"
    }
});


/**
 * Komponenta slouží jako tlačítko, kterému se předá funkce, ikona, alt - popisek, tooltip atd.
 */

class ImageButton extends React.Component {

    render() {
        return (
            <Tooltip title={this.props.tooltip}>
                <IconButton onClick={this.props.onClick} color={this.props.color} alt={this.props.alt}
                            className={this.props.classes.button} aria-label={this.props.alt}>
                    <img src={this.props.image} alt={this.props.alt}/>
                </IconButton>
            </Tooltip>);
    }
}


ImageButton.propTypes = {
    classes: PropTypes.object.isRequired
};


export default withStyles(styles)(ImageButton);