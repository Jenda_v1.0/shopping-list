import React from 'react';
import './styles/Loading.css'
// https://reactjsexample.com/a-collection-of-loading-spinner-components-for-react/
import {PacmanLoader} from 'react-spinners';


export default class Loading extends React.Component {

    render() {
        return (
            <div className="loading">
                <PacmanLoader
                    sizeUnit={"px"}
                    // Velikost obrázku:
                    size={25}
                    // Barva obrázku:
                    color={'#175dbc'}
                    loading={true}
                />
            </div>
        );
    }
}