import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';
import ImageButton from './ImageButton';
import DeleteIcon from '../images/delete.png';
import EditIcon from '../images/editItem.png';
import Grid from '@material-ui/core/Grid';
import './styles/ExpansionPanelItem.css';


/**
 * Komponenta slouží pro zobrazení uživatelem vytvořených položek. Každá položka je zobrazena jako "ExpansionPanel", ve kterém jsou zobrazeny zadané infromace o položce.
 */

export default class ExpansionPanelItem extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            // true = záložka je otevřená, false - není otevřená:
            expanded: false
        };
    }


    // Nastavení, zdali je rozevírací panel pro zobrazení podrobností o položce otevřen nebo ne:
    handleExpand = () => {
        this.setState({expanded: !this.state.expanded});
    };


    // Získání "normálního textu" s názvem položky (bez libovolných úprav stylování apod.):
    getDefaultItemName = () => {
        return <div className="Heading">{this.props.item.name}</div>
    };


    // Zjištění, zdali je vlastnost "property" uvedená / zadaná:
    isValuePresent = (property) => {
        return (property !== undefined && property !== "")
    };


    // Převod datumu dateTime do textové podoby pro zobrazení uživateli, z textu vezme poue text o délce od začátku do length:
    dateTimeToText = (dateTime, length) => {
        return dateTime.toLocaleString().substr(0, length);
    };


    // Zjištění délky datumu (po převedení do textu), který se má z toho textu ořiznout, aby nebyly přitomny milisekundy, dvojtečky apod.
    getDateTimeLength = date => {
        // Výchozí délka datumu po převedení do textu, který se má vzít:
        let length = 18;

        // (lze napsat jako jednu podmínku - pouze pro přehlednost)

        if (date.getDate() < 10)
            length--;

        if (date.getMonth() + 1 < 10)
            length--;

        if (date.getHours() < 10)
            length--;

        return length;
    };


    render() {
        // Test, "jak" se má zobrazit název položky. Název bude obarven na červeno v případě, že datum splnění položky je starší než aktuální datum (položka již měla být splněna). V opačném případě bude název položky obarven "běžnou" barvou - černou:
        // (Podmínku by bylo možné spojit s testování deadline, ale zde je to přehlednější a oddělena logika)
        let name;

        if (this.isValuePresent(this.props.item.deadline)) {
            let date = new Date(this.props.item.deadline);

            // V případě, že je datum položky starší než aktuální datum, obarví se titulek / název položky na červeno:
            if (date <= new Date() && !this.props.item.done) {
                name = <div className="InvalidHeading">
                    {this.props.item.name}
                </div>
            } else name = this.getDefaultItemName();
        } else name = this.getDefaultItemName();


        // Poznámka bude zobrazena pouze v případě, že je nějaká zadána:
        let note;
        if (this.isValuePresent(this.props.item.note)) {
            note = <div className="Detail">
                <div className="DetailNoteHeadline"><b>Note:</b></div>
                {this.props.item.note}
            </div>
        }


        // Množství bude zobrazeno pouze v případě, že je zadáno:
        let amount;
        if (this.isValuePresent(this.props.item.amount)) {
            amount = <div className="Detail">
                <b>Amount:</b> {this.props.item.amount} {this.props.item.kindOfUnit}
            </div>
        }


        let deadline;
        if (this.isValuePresent(this.props.item.deadline)) {
            let date = new Date(this.props.item.deadline);

            // Získání délky (počtu znaků) pro oříznutí, aby nebyly přítomny, poslední dvojtečky, milisekundy apod.:
            let dateTimeLength = this.getDateTimeLength(date);

            if (date <= new Date() && !this.props.item.done) {
                // Zde je datum staré (starší než aktuální datum) a položka ještě není splněna, tak se zvýrazní i datum, aby si uživatel všíml, že je datum prošlé, jinak by viděl pouze název položky obarvený na červeno, to by na první pohled nemuselo dávat smysl:
                deadline = <div className="DetailInvalidDeadline">
                    {/*Vezme se datum a čas, čas bude na konci bez vteřin (proto substr):*/}
                    <b>Deadline:</b> {this.dateTimeToText(date, dateTimeLength)}
                </div>
            } else {
                // Zde je datum ještě v pořádu (není "prošlý"), tak se zobrazí bez nějakého zvýraznění:
                deadline = <div className="Detail">
                    {/*Vezme se datum a čas, čas bude na konci bez vteřin (proto substr):*/}
                    <b>Deadline:</b> {this.dateTimeToText(date, dateTimeLength)}
                </div>
            }
        }


        // Zjištění, jestli je uvedena priorita. Výchozí je "none", tzn. bez priority - nebude zobrazena. V ostatních případech byla nastavena priorita:
        // ("none" je výchozí hodnota v FormItem, značí bez uvedení priority - hlídat v případě změny)
        // showPriority je třeba proto, aby když se jedná o záložku s prioritami, kde se položky filtrují dle požadované priority, tak tam není potřeba zobrazit prioritu - byly by vždy stejné
        let priority;
        if (this.isValuePresent(this.props.item.priority) && this.props.item.priority !== "none" && this.props.showPriority) {
            priority = <div className="Detail">
                <b>Priority:</b> {this.props.item.priority}
            </div>
        }


        // Zjistí se, jestli je alespoň jedna z podrobností zadána, pokud ne, bude zobrazena pouze tato hláška o tom, že nebyly poskytnuty žádné podrobnosti:
        let details;
        if (note === undefined && amount === undefined && deadline === undefined && priority === undefined) {
            details = <div className="Detail">
                No details were provided.
            </div>
        }


        return (
            <div className="root">
                <ExpansionPanel expanded={this.state.expanded}>
                    {/*Událost otevření / zavření panelu je pouze na ikonu - kvůli chcb, jinak by to reagovalo i při kliknutí na něj:*/}
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon onClick={this.handleExpand}/>}>
                        {/*Chcb pro nastavení, jestli je položka splněna nebo ne:*/}
                        <Checkbox
                            checked={this.props.item.done}
                            onChange={this.props.changeDoneStatusItem}
                            color="primary"
                        />

                        {/*Název položky:*/}
                        {name}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Grid container spacing={40} alignContent='center' alignItems='center' item={true}>
                            {/*Informace o tom, že nebyly zadány žádné podrobností k položce:*/}
                            {details}

                            {/*Poznámka:*/}
                            {note}

                            {/*Množství:*/}
                            {amount}

                            {/*Termín splnění položky:*/}
                            {deadline}

                            {/*Priorita položky:*/}
                            {priority}

                            {/*Editace položky:*/}
                            <ImageButton className="ImageButton" onClick={this.props.setItemForEdit} color="inherit"
                                         image={EditIcon} alt={"Edit item"} tooltip={"Edit"}/>

                            {/*Odstranění položky:*/}
                            <ImageButton className="ImageButton" onClick={this.props.deleteItem} color="secondary"
                                         image={DeleteIcon} alt={"Delete item"} tooltip={"Delete"}/>
                        </Grid>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </div>
        );
    }
}