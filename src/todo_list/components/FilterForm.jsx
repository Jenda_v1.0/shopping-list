import React from 'react';
import TextField from '@material-ui/core/TextField';
import SelectTextField from "./SelectTextField";
import {FILTER_BY} from '../constants/FilterBy';
import './styles/FilterForm.css';


/**
 * Formulář pro filtrování vytvořených položek.
 */

export default class FilterForm extends React.Component {

    render() {
        return (
            <div>
                {/*Pole pro filtrování vytvořených položek na záložkách:*/}
                <TextField
                    id="itemFilter"
                    label="Filter"
                    placeholder="Value"
                    helperText="Value for filtering"
                    margin="normal"
                    variant="filled"// šedivé poličko, další: standard, outlined
                    InputLabelProps={{shrink: true}}
                    onChange={this.props.setAndFilterValues}
                    className="TextFieldFilter"
                />


                {/*Textové pole typu select pro zvolení "dle čeho" se mají položky filtrovat*/}
                <SelectTextField label={"Filter by"} value={this.props.filterByValue}
                                 handleChange={this.props.setAndFilterByValues}
                                 helperText={"Filter items by text"}
                                 items={FILTER_BY} width={90}/>
            </div>
        );
    }
}