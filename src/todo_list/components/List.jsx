import * as React from "react";
import FormItem from "./FormItem";
import TabsItem from './TabsItem';
import Calls from '../server/calls';
import FilterForm from './FilterForm';
import {FILTER_BY} from '../constants/FilterBy';
import Loading from "./Loading";
import * as LOADING from '../constants/Loading';


/**
 * Komponenta obsahující formulář pro vytvoření nové položky a pod ním TabsItem, což jsou záložky filtrující jednotlivé položky, které by měl uživatel vykonat (které uživatel vytvořil). (Komponenta TabsItem je vykreslena zde, aby se ušetřilo předávání hodnot.)
 *
 * Tato komponenta obsahuje pole items obsahující vytvořené položky, dále CRUD operace nad nimi.
 */

export default class List extends React.Component {

    constructor(props) {
        super(props);

        // Položky v listu:
        this.state = {
            items: [],
            // Hodnota v textovém poli pro filtrování položek:
            filterValue: "",
            // Hodnota zvolená v picker pro typ hodnoty, dle které se mají filtrovat položky:
            filterBy: FILTER_BY[0].value,
            // Fáze načítání dat:
            loading: "Init",
            // Info o nastalé chybě:
            error: "",
            // Velikost některých komponent, kterým je třeba nastavit respozivni velikost:
            responsiveWidth: 0,
            // Položka, kterou chce uživatel upravit (pro natavení hodnot do formuláře):
            itemForEdit: undefined
        };

        // Reference na komponentu pro zavolání metody z předka do potomka (pro nastavení hodnot ve formuláři):
        // https://stackoverflow.com/questions/37949981/call-child-method-from-parent
        this.formItemRef = React.createRef();
    }


    // Nastavení fáze pro znázornění animace s načítáním:
    setLoadingState = state => {
        this.setState({loading: state});
    };


    // Nastavení fáze značící chybu:
    setLoadingStateError = error => {
        this.setState({error: error, loading: LOADING.LOADING});
    };


    // Nastavení aktuální šířky okna:
    updateWindowWidth = () => {
        this.setState({responsiveWidth: window.innerWidth});
    };


    // Výchozí načtení hodnot z DB:
    async componentDidMount() {
        // Toto je třeba nastavit pro přechod na novou verzi:
        // https://material-ui.com/style/typography/#migration-to-typography-v2
        window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;


        // Přidání události na změnu velikosti okna obrazovky:
        // https://stackoverflow.com/questions/19014250/rerender-view-on-browser-resize-with-react
        window.addEventListener("resize", this.updateWindowWidth);
        // Zrovna se nastaví aktuální velikost pro výchozí zobrazení komponent:
        this.updateWindowWidth();

        // Načtení dat z DB:
        this.setLoadingState(LOADING.LOADING);
        try {
            const result = await Calls.getShoppingList();
            this.setState({items: result});
            this.setLoadingState(LOADING.DONE);
        } catch (error) {
            this.showError("Error while loading data from DB: " + error);
            this.setLoadingStateError(error);
        }
    }


    // Zobrazení hlášky uživateli:
    showError = message => {
        console.log(message);
        alert(message);
    };


    // Načtení veškerých hodnot / položek z DB:
    loadItems = async () => {
        this.setLoadingState(LOADING.LOADING);
        await Calls.getShoppingList()
            .then(items => {
                this.setState({items: items});
                this.setLoadingState(LOADING.DONE);
            })
            .catch(error => {
                this.showError("Error while loading data from DB: " + error);
                this.setLoadingStateError(error);
            });
    };


    // Získání položky s id z pole items:
    getItemById = id => {
        return this.state.items.find(function (item) {
            return item.id === id;
        });
    };


    // Odstranění položky s id v poli items (pole všech položek):
    deleteItem = async id => {
        // Nalezení jedné konkrétní položky s konkrétním ID:
        const item = this.getItemById(id);

        // Podmínka by neměla být nikdy splněna:
        if (item === undefined) {
            const error = "Item with id: '" + id + "' was not found !";
            this.showError(error);
            return;
        }

        // Dotaz na potvrzení smazání položky:
        if (!window.confirm("Are you sure you want to delete the '" + item.name + "' ?"))
            return;

        // Smazání položky z DB:
        await Calls.deleteShoppingItem(item)
            .then(async () => {
                await this.loadItems();
            })
            .catch(error => {
                this.showError("Error while deleting item from DB.\nItem: " + item + "\n" + error);
            });
    };


    // Odstranění veškerých položek:
    deleteAllItems = async () => {
        // Položky má smysl smazat pouze v případě, že alespoň jedna existuje, jina nemá smysl se ani ptát apod.:
        if (this.state.items.length === 0)
            return;

        // Dotaz na potvrzení, zdali se mají smazat veškeré položky v DB:
        if (!window.confirm("Are you sure you want to delete all items ?"))
            return;

        await this.setLoadingState(LOADING.LOADING);
        await this.state.items.forEach((item) => {
            Calls.deleteShoppingItem(item)
                .catch(error => {
                    this.showError("Error while deleting all items from DB: " + error);
                    this.setLoadingStateError(error);
                });
        });

        await this.setState({items: []});
        await this.setLoadingState(LOADING.DONE);
    };


    // Nastavení, jestli je Item splněna nebo ne:
    changeDoneStatusItem = id => async event => {
        // Získání položky z pole:
        const updateItem = this.getItemById(id);
        // Nastavení splnění položky:
        updateItem.done = event.target.checked;

        // Aktualizace položky:
        await this.updateItem(updateItem);
    };


    // Aktualizace položky v poli a poté v DB:
    updateItem = async editedItem => {
        // Aktualizace položky v poli (aby se nemuseli přenačítat veškerá data v DB):
        await this.setState({
            items: this.state.items.map(item => (item.id === editedItem.id ? Object.assign({}, item, editedItem) : item))
        });

        // Aktualizace položky v DB:
        // (Volá se pouze při změně stavu splnění položky, proto není třeba překreslovat stránku a znovu načítat všechna data.)
        await this.updateItemInDB(editedItem);
    };


    // Aktualizace položky v DB:
    updateItemInDB = async newItem => {
        await Calls.updateShoppingItem(newItem)
            .catch(error => {
                this.showError("Error while updating item.\nItem: " + newItem + "\n" + error);
                this.setLoadingStateError(error);
            });
    };


    // Vytvoření nové položky:
    addItem = async formState => {
        // vytvoření nové položky s nastavenými / výchozími hodnotami:
        const newItem = {
            done: false,// výchozí hodnota je false, tj. při vytvoření položky nebude splněna
            name: formState.valueInTextInputItemName,
            note: formState.multilineNote,
            amount: formState.amount,
            unit: formState.unit,
            kindOfUnit: formState.kindOfUnit,
            deadline: formState.deadline,
            priority: formState.priority
        };


        // Nová položka se vloží do DB a vrátí se s vygenerovaným ID:
        const newServerItem = await Calls.createShoppingItem(newItem).catch(error => {
            this.showError("Error while creating item in DB: " + error);
        });

        // Aby se nemusely načítat všechny nové položky z DB:
        let items = [...this.state.items];
        items.push(newServerItem);
        await this.setState({items: items});
    };


    // Nastavení položky s id pro editaci, tj. že se nastaví hodnoty položky do formuláře:
    setItemForEdit = async id => {
        const itemById = this.getItemById(id);
        await this.setState({itemForEdit: itemById});

        // Nastavení dat do formuláře:
        await this.formItemRef.current.setDataToForm(itemById);
    };


    // Zjištění, jestli již položka s textem v parametru "text" existuje nebo ne:
    existItem = text => {
        return this.state.items.filter((item) => item.name === text).length > 0;
    };


    // Zjištění, jestli již položka s textem v parametru "text" existuje nebo ne:
    // Ale vynechá se testování položky s příslušným id:
    checkExistItem = (text, id) => {
        return this.state.items.filter((item) => item.name === text && item.id !== id).length > 0;
    };


    // Nastavení atributu na hodnotu, která se má vyhledávat nebo "dle které hodnoty" se má vyhledávat v položkách:
    setAndFilterValues = name => event => {
        this.setState({[name]: event.target.value});
    };


    // Získání komponent, které se mají vykreslit:
    getComponentsForRender = () => {
        switch (this.state.loading) {
            case LOADING.LOADING:
                return <Loading/>;


            case LOADING.DONE:
                return (
                    <React.Fragment>
                        {/*Formulář pro vytvoření nové položky:*/}
                        <FormItem addItem={this.addItem} items={this.state.items} existItem={this.existItem}
                                  deleteAllItems={this.deleteAllItems} responsiveWidth={this.state.responsiveWidth}
                                  ref={this.formItemRef} updateItem={this.updateItem}
                                  checkExistItem={this.checkExistItem}/>

                        <br/>

                        {/*Komponenty pro filtrování položek:*/}
                        <FilterForm setAndFilterValues={this.setAndFilterValues("filterValue")}
                                    setAndFilterByValues={this.setAndFilterValues("filterBy")}
                                    filterByValue={this.state.filterBy}/>

                        <br/>

                        {/*Záložky s vytvořenými položkami:*/}
                        <TabsItem filterValue={this.state.filterValue} items={this.state.items}
                                  filterByValue={this.state.filterBy} changeDoneStatusItem={this.changeDoneStatusItem}
                                  deleteItem={this.deleteItem} responsiveWidth={this.state.responsiveWidth}
                                  setItemForEdit={this.setItemForEdit}/>
                    </React.Fragment>
                );


            case LOADING.ERROR:
                return <div>Error: {this.state.error}</div>;


            default:
                return <div>Unknown Error.</div>;
        }
    };


    render() {
        return <div>{this.getComponentsForRender()}</div>;
    }
}