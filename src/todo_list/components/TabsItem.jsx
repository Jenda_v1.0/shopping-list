import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import ExpansionPanelItem from './ExpansionPanelItem';
import SelectTextField from "./SelectTextField";
import {PRIORITIES} from "../constants/Priorities";
import './styles/TabsItem.css'


function TabContainer({children, dir}) {
    return (
        <Typography component="div" dir={dir} style={{padding: 8 * 3}}>
            {children}
        </Typography>
    );
}


TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired
};


const styles = ({});


const priorityDefault = PRIORITIES[0].value;


/**
 * Komponenta slouží jako jednotlivé záložky pod formulářem pro vytvoření nové položky. Na každé záložce se filtrují vytvořené položky dle vybraných kritérií.
 */

class TabsItem extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            // Index označené záložky:
            value: 0,
            // Výchozí zobrazená priorita pro filtrování položek:
            priority: priorityDefault
        };
    }


    handleChange = (event, value) => {
        this.setState({value});
    };


    handleChangeIndex = index => {
        this.setState({value: index});
    };


    // Nastavení proměnné "priority" na zvolenou hodnotu v chcb. Jedná se o hodnotu, dle které se mají vyfiltrovat položky:
    handleChangePriority = event => {
        this.setState({priority: event.target.value});
    };


    // Získání / vyfiltrování položek, které mají prioritu "priority" v parametru funkce:
    getItemsWithPriority = priority => {
        return this.props.items.filter((item) => item.priority === priority).map(item => (
            this.getExpansionPanel(item, false)
        ));
    };


    // Získání / vyfiltrování položek, které mají prioritu "priority" v parametru funkce a zároveň jejich název obsahuje hodnotu v filterValue:
    getItemsWithPriorityAndFiltering = (priority, filterValue) => {
        return this.filter(this.props.items, {[this.props.filterByValue]: filterValue}).filter((item) => item.priority === priority).map(item => (
            this.getExpansionPanel(item, true)
        ));
    };


    // Vytvoření a vrácení komponenty ExpansionPanelItem, které se předají potřebné hodnoty do props. Komponenta zobrazuje informace o vytvořených Items:
    // showPriority se předává, aby se poznalo, jestli se má priorita zobrazit. V případě, že se jedná o filtrování položek na záložce s prioritami, tak tam se nemusí zobrazovat.
    getExpansionPanel = (item, showPriority) => {
        return (
            <ExpansionPanelItem
                key={item.id}
                item={item}
                showPriority={showPriority}
                changeDoneStatusItem={this.props.changeDoneStatusItem(item.id)}
                // "Předání zavolání fce":
                deleteItem={() => this.props.deleteItem(item.id)}
                setItemForEdit={() => this.props.setItemForEdit(item.id)}
            />
        );
    };


    // Vyfiltrování položek v poli items, filtrují se dle kritérií v criteria = název atributu a jeho hodnota (testuje se pouze, zda hodnota obsahuje text v criteria - metoda "includes"):
    // Zdroj: https://stackoverflow.com/questions/17099029/how-to-filter-a-javascript-object-array-with-variable-parameters
    filter(items, criteria) {
        return items.filter(function (item) {
            return Object.keys(criteria).every(function (c) {
                return item[c].includes(criteria[c]);
            });
        });
    }


    // Získání responzivní šířky komponenty:
    getResponsiveWidth = () => {
        const size = this.props.responsiveWidth;
        let responsiveSize = 290;

        if (size <= 385)
            responsiveSize = 175;

        return responsiveSize;
    };


    render() {
        // Deklarace proměnných pro vložení vyfiltrovaných položek (dle hodnoty pro filtrování nebo zdali jsou splněny apod.):
        let allItems, activeItems, completedItems, itemsWithPriority;

        // Hodnota z textového pole pro vyfiltrování položek dle jejich názvu:
        let valueForFilter = this.props.filterValue;

        // V případě, že je zadán nějaký text do pole, budou se položky filtrovat dle zadaného názvu a případně, jestli jsou splněny apod.
        if (valueForFilter !== undefined && valueForFilter !== "") {
            // Veškeré vytvořené položky:
            allItems = this.filter(this.props.items, {[this.props.filterByValue]: valueForFilter}).map(item => (
                this.getExpansionPanel(item, true)
            ));

            // Veškeré aktivní poloky, tedy ty, které ještě nejsou splněny:
            activeItems = this.filter(this.props.items, {[this.props.filterByValue]: valueForFilter}).filter((item) => !item.done).map(item => (
                this.getExpansionPanel(item, true)
            ));

            // Veškeré položky, které již josu splněny / hotové položky:
            completedItems = this.filter(this.props.items, {[this.props.filterByValue]: valueForFilter}).filter((item) => item.done).map(item => (
                this.getExpansionPanel(item, true)
            ));

            // Vyfiltrování položek s nastavenou prioritou:
            itemsWithPriority = this.getItemsWithPriorityAndFiltering(this.state.priority, valueForFilter);
        } else {
            // Zde se nebudou filtrovat položky dle zadané hodnoty do pole pro filtrování, pouze dle toho, zdali jsou splněny nebo ne - dle záložky:

            // Veškeré vytvořené položky:
            allItems = this.props.items.map(item => (
                this.getExpansionPanel(item, true)
            ));

            // Veškeré aktivní poloky, tedy ty, které ještě nejsou splněny:
            activeItems = this.props.items.filter((item) => !item.done).map(item => (
                this.getExpansionPanel(item, true)
            ));

            // Veškeré položky, které již josu splněny / hotové položky:
            completedItems = this.props.items.filter((item) => item.done).map(item => (
                this.getExpansionPanel(item, true)
            ));

            // Vyfiltrování položek s nastavenou prioritou:
            itemsWithPriority = this.getItemsWithPriority(this.state.priority);
        }


        // Názvy záložek s existujícími položkami, za názvem záložky bude počet položek, které se v ní nachází:
        const tabAllItems = "All (" + allItems.length + ")";
        const tabActiveItems = "Active (" + activeItems.length + ")";
        const tabCompletedItems = "Completed (" + completedItems.length + ")";
        const tabItemsByPriority = "Priority (" + itemsWithPriority.length + ")";


        // Nastavení responzivní velikosti pro komponentu pro zvolení priority pro filtrování:
        const responsiveWidth = this.getResponsiveWidth();


        return (
            <div className="Root">
                <AppBar position="static" color="default">
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        fullWidth
                        scrollable
                        scrollButtons="auto"
                    >
                        <Tab label={tabAllItems}/>
                        <Tab label={tabActiveItems}/>
                        <Tab label={tabCompletedItems}/>
                        <Tab label={tabItemsByPriority}/>
                    </Tabs>
                </AppBar>


                <SwipeableViews
                    axis={this.props.theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={this.state.value}
                    onChangeIndex={this.handleChangeIndex}
                >

                    {/*Veškeré vytvořené položky:*/}
                    <TabContainer dir={this.props.theme.direction}>
                        {allItems}
                    </TabContainer>

                    {/*Položky, které ještě nebyly splněny - jsou aktivní:*/}
                    <TabContainer dir={this.props.theme.direction}>
                        {activeItems}
                    </TabContainer>

                    {/*Položky, které jsou hotové - byly označeny jako splněny:*/}
                    <TabContainer dir={this.props.theme.direction}>
                        {completedItems}
                    </TabContainer>

                    {/*Položky, pro seřazení filtrování priority:*/}
                    <TabContainer dir={this.props.theme.direction}>
                        <SelectTextField label={"Priority"} value={this.state.priority}
                                         handleChange={this.handleChangePriority}
                                         helperText={"Filtering priority items"} items={PRIORITIES}
                                         width={responsiveWidth}/>
                        {/*Vyfiltrované položky:*/}
                        {itemsWithPriority}
                    </TabContainer>
                </SwipeableViews>
            </div>
        );
    }
}


TabsItem.propTypes = {
    theme: PropTypes.object.isRequired
};


export default withStyles(styles, {withTheme: true})(TabsItem);