// Priorita položky (do určité míry lze brát jako kategorie):

// ("none" je výchozí hodnota bez priority, v případě změny změnit i testování v ExpansionPanelItem.)

// (První hodnota je brána jako výchozí priorita - none bez uvedení priority)

export const PRIORITIES = [
    {
        value: "none",
        label: "none"
    },
    {
        value: "must be fulfilled",
        label: "must be fulfilled"
    },
    {
        value: "necessarily",
        label: "necessarily"
    },
    {
        value: "good to meet",
        label: "good to meet"
    },
    {
        value: "hurries",
        label: "hurries"
    },
    {
        value: "does not hurry",
        label: "does not hurry"
    },
    {
        value: "when it is fulfilled it will be fulfilled",
        label: "when it is fulfilled it will be fulfilled"
    }
];