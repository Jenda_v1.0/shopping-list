// Typy hodnot, dle kterých lze filtrovat vytvořené položky, například text v názvu, v poznámce apod.

// (První hodnota je brána jako výchozí typ hodnoty, dle které se budou filtrovat položky:

export const FILTER_BY = [
    {
        value: "name",
        label: "name"
    },
    {
        value: "note",
        label: "note"
    }
];