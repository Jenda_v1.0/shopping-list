// Konstanty značí typy jednotek / množství. Například 5 kusů jako 5 rohlíků apod.

// (První hodnota je brána jako výchozí typ hodnoty, dle které se budou filtrovat položky:

export const UNITS = [
    {
        value: "piece",
        label: "ks"
    },
    {
        value: "kilogram",
        label: "kg"
    },
    {
        value: "decagram",
        label: "dkg"
    },
    {
        value: "gramme",
        label: "g"
    },
    {
        value: "pound",
        label: "lb"
    }
];